from aiogram import types, Dispatcher
from keyboards import kb_client
from creat_bot import bot
from API import get_link_to_pdf


async def command_start(message: types.Message):
    try:
        await bot.send_message(message.from_user.id, 'Send me any word and I will give you a link to a scientific article!', reply_markup=kb_client)
        await message.delete()
    except:
        await message.reply('Communication with the bot via, write to the bot in person')


async def echo_send(message: types.Message):
    if len(message.text.split(" ")) > 1:
        await message.answer("Please enter one word")
    link = get_link_to_pdf(message.text)
    if link[0] != 0:
        await message.answer(link[1])
        await message.answer(link[0])
    else:
        await message.answer("Sorry, nothing was found for this word(")


def register_handlers_users(dp: Dispatcher):
    dp.register_message_handler(command_start, commands=['start', 'help'])
    dp.register_message_handler(echo_send)