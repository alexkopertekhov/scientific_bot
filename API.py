import feedparser
from random import randint
from globals import base_url


# feedparser v4.1


def get_link_to_pdf(query_word):
    list_links_to_pdf = list()
    list_titles = list()
    # Search parameters
    search_query = 'all:' + str(query_word)  # search for query_word in all fields

    start = randint(0, 20)  # retreive 10 results
    max_results = start + 10
    query = 'search_query=%s&start=%i&max_results=%i' % (search_query,
                                                         start,
                                                         max_results)

    feed = feedparser.parse(base_url + query)

    for entry in feed.entries:
        for link in entry.links:
            # print('Title:  %s' % entry.title)
            if link.rel == 'alternate':
                continue
            elif link.title == 'pdf':
                list_links_to_pdf.append(link.href)
                list_titles.append('Title:  %s' % entry.title)
                # print('pdf link: %s' % link.href)

    if len(list_links_to_pdf) == 0:
        return (0, 0)

    i = randint(0, 9)
    return (list_links_to_pdf[i], list_titles[i])
