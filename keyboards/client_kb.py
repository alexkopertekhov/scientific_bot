from aiogram.types import ReplyKeyboardMarkup, KeyboardButton, ReplyKeyboardRemove

b_help = KeyboardButton('/help')
b_physics = KeyboardButton("phys")
b_mathematics = KeyboardButton("math")
b_computer_science = KeyboardButton("comp")
b_quantitative_biology = KeyboardButton("bio")
b_quantitative_finance = KeyboardButton("fin")
b_statistics = KeyboardButton("stat")
b_electrical_engineering_and_systems_science = KeyboardButton("electr")
b_economics = KeyboardButton("econ")

kb_client = ReplyKeyboardMarkup(resize_keyboard=True)

kb_client.row(b_help, b_physics, b_mathematics, b_computer_science,
              b_quantitative_biology, b_quantitative_finance,
              b_statistics, b_electrical_engineering_and_systems_science,
              b_economics)
